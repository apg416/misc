def encode(mes):
    num=[]
    n=0
    p=0
    for char in mes:
        num.append(ord(char)-32)
        n=n+1
    for i in xrange(n):
        p=p+(num[i]*(10**(2*(n-i-1))))
    return p
    
def decode(p):
    k=p
    n=0
    mes=''
    while(k!=0):
        k=k/100
        n=n+1
    for j in xrange(n):
        i=n-1-j
        l=p/100**i
        mes=mes+chr(l+32)
        p=p-l*(100**i)
    return mes
    
def cipher(mes,e,n):
    p=encode(mes)
    c=pow(p,e,n)
    return c
    
def decipher(c,d,n):
    p=pow(c,d,n)
    mes=decode(p)
    return mes