from random import randint

def milrab (n,a):
    d=n-1
    s=0
    while d%2==0:
        d=d/2
        s=s+1
    res=[]
    for r in xrange(s):
        x=pow(a,(2**r)*d,n)
        res.append(x)
    return res
    
def miller_rabin_test(n,a):
    d=n-1
    s=0
    while d%2==0:
        d=d/2
        s=s+1
    if pow(a,d,n)==1:
        return True
    for r in xrange(s):
        x=pow(a,(2**r)*d,n)
        if x==n-1:
            return True
    return False

def isprime(n):
    for i in xrange(10):
        a=randint(2,n-2)
        if miller_rabin_test(n,a)==False:
            return False
    return True

def nextprime(n):
    p=n+1
    while isprime(p)==False:
        p=p+1
    return p