#Below is just code with some brief comments, you can find more detailed
#description of this script in IPythonNotebook.

import matplotlib.pyplot as plt
import numpy as np

# escape_time and mandelbrot_array from Question 1

def escape_time(c, nmax):
    i=0
    val=0
    while(abs(val)<=2 and i<nmax):
        val=(val*val)+c
        i=i+1
    return i

def mandelbrot_array(xmin, xmax, ymin, ymax, npoints):
    # calculate step sizes
    xstep = (xmax - xmin)/float(npoints - 1)
    ystep = (ymax - ymin)/float(npoints - 1)
    # initialize array
    et_array = np.zeros([npoints, npoints])
    # nested loop
    for x_index in xrange(npoints):
        # real part of c
        x = xmin + x_index * xstep
        for y_index in xrange(npoints):
            # imaginary part of c
            y = ymin + y_index * ystep
            # calculate c
            c = x+y*1j
            # calculate log of escape time and drop into array
            log_et = np.log(escape_time(c, 200))
            et_array[npoints-y_index-1, x_index] = log_et
    return et_array

#---------Question 3------------

import pickle

#initialize
initial_xmin=-1.5
initial_xmax=0.5
initial_ymin=-1.0
initial_ymax=1.0
centre_x=0.133
centre_y=0.595


#creating frames for animation
for frame_index in xrange(100):
    
    #set coordinates
    xmin = centre_x + (initial_xmin - centre_x) * 0.9 ** frame_index
    xmax = centre_x + (initial_xmax - centre_x) * 0.9 ** frame_index
    ymin = centre_y + (initial_ymin - centre_y) * 0.9 ** frame_index
    ymax = centre_y + (initial_ymax - centre_y) * 0.9 ** frame_index
    
    #calculate array
    ar=mandelbrot_array(xmin, xmax, ymin, ymax, 21)
    
    #creating name of the file containing its index number
    name='mandelbrotframe'+str(frame_index)+'.pickle'
    
    #saving
    op=open(name,"wb")
    pickle.dump(ar,op)
    op.close()
    
for frame_index in xrange(100):
    
    #load frame with appropriate index
    name='mandelbrotframe'+str(frame_index)+'.pickle'
    ip=open(name,"rb")
    array=pickle.load(ip)
    ip.close()
    
    #plotting
    plt.imshow(array)
    plt.show()
    plt.pause(0.01)
    if frame_index<99:
        plt.cla()
    